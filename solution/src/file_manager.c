#include "file_manager.h"

enum file_status open_file(FILE **file, const char *file_name, char const *mode) {
	*file = fopen(file_name, mode);

	if (*file == NULL) {
		return OPEN_ERROR;
	}
	return OPEN_OK;
}

void close_file(FILE *file) {
	fclose(file);
}
