#ifndef ROTATER_H
#define ROTATER_H

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

struct image *rotate(struct image const *source);

#endif
