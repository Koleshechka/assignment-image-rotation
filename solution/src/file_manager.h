#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include <stdio.h>

enum file_status {
	OPEN_OK = 0,
	OPEN_ERROR
};

enum file_status open_file(FILE **file, const char *file_name, char const *mode);

void close_file(FILE *file);

#endif
