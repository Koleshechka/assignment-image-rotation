#include <inttypes.h>
#include <stdio.h>

#include "bmp.h"
#include "image.h"

#define HEADER_TYPE 0x4D42;
#define HEADER_RESERVED 0;
#define HEADER_BITS 54;
#define HEADER_SIZE 40;
#define HEADER_PLANES 1;
#define HEADER_BIT_COUNT 24;
#define HEADER_COMPRESSION 0;
#define HEADER_X_PELS_PER_METER 0;
#define HEADER_Y_PELS_PER_METER 0;
#define HEADER_CLR_USED 0;
#define HEADER_CLR_IMPORTANT 0;

static uint8_t get_padding(const uint8_t width) {
	return (4 - (3 * width % 4)) % 4;
}

enum read_status from_bmp( FILE* in, struct image** img ) {
	bmpHeader header;

	if (!fread(&header, sizeof(bmpHeader), 1, in)) {
		return READ_INVALID_HEADER;
	}
	
	struct image* image =  create_image(header.biHeight, header.biWidth);
	uint8_t padding = get_padding(header.biWidth);

	struct pixel *pixel = malloc(sizeof(struct pixel));
	for (size_t i = 0; i < header.biHeight; i++){
    		for (size_t j = 0; j < header.biWidth; j++) {
      			fread(pixel, sizeof(struct pixel), 1, in);
      			image->data[header.biWidth * i + j] = *pixel;
    		}

    		if (padding != 0){
      		fread(pixel, sizeof(unsigned char)*padding, 1, in);
    		}
  	}
	free(pixel);
	*img = image;
	return READ_OK;

}

enum write_status to_bmp( FILE* out, struct image const* img ) {
	uint8_t padding = get_padding(img->width);	
	
	bmpHeader bmpHeader={0};
	bmpHeader.bfType = HEADER_TYPE;
  	bmpHeader.bfReserved = HEADER_RESERVED;
  	bmpHeader.bOffBits = HEADER_BITS;
  	bmpHeader.biSize = HEADER_SIZE;
  	bmpHeader.biWidth = img->width;
  	bmpHeader.biHeight = img->height;
  	bmpHeader.biPlanes = HEADER_PLANES;
  	bmpHeader.biBitCount = HEADER_BIT_COUNT;
  	bmpHeader.biCompression = HEADER_COMPRESSION;
  	bmpHeader.biSizeImage = (img->width * 3 + padding) * img->height;
  	bmpHeader.bfileSize = bmpHeader.biSizeImage + sizeof(bmpHeader);
  	bmpHeader.biXPelsPerMeter = HEADER_X_PELS_PER_METER;
  	bmpHeader.biYPelsPerMeter = HEADER_Y_PELS_PER_METER;
  	bmpHeader.biClrUsed = HEADER_CLR_USED;
  	bmpHeader.biClrImportant = HEADER_CLR_IMPORTANT;

	if (!fwrite(&bmpHeader, sizeof(bmpHeader), 1, out)) {
		return WRITE_ERROR;
	}
  	fseek(out, bmpHeader.bOffBits, SEEK_SET);
	
	struct pixel *pixel = malloc(sizeof(struct pixel));
	for (size_t i = 0; i < img->height; i++) {
    		for (size_t j = 0; j < img->width; j++) {
     			*pixel = img->data[img->width * i + j];
      			fwrite(pixel, sizeof(struct pixel), 1, out);
    		}
    		if (padding != 0) {
      			for (size_t j = 0; j < padding; j++) {
        			pixel->b = 0;
        			pixel->r = 0;
        			pixel->g = 0;
        			fwrite(pixel, sizeof(unsigned char), 1, out);
      			}
    		}
  	}
	free(pixel);
	return WRITE_OK;
}
