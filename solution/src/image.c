#include "image.h"

struct image* create_image(uint64_t height, uint64_t width) {
	struct image *image = malloc(sizeof(struct image));
	image->width = width;
    	image->height = height;
    	image->data = malloc(sizeof(struct pixel) * width * height * 3);
   	return image;

}

void delete_image (struct image* image) {
	free(image->data);
	free(image);
}
