#ifndef IMAGE_H
#define IMAGE_H
#include <inttypes.h>
#include <stdlib.h>

struct pixel {
	uint8_t r, g, b;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image* create_image(uint64_t height, uint64_t width);

void delete_image (struct image* image);
#endif
