#include "rotater.h"
#include "bmp.h"
#include "file_manager.h"
#include <stdlib.h>

void print_error(char* error) {
	fprintf(stderr, "%s\n", error);
}

int main( int argc, char** argv ) {
	if (argc != 3) {
		printf("Incorrect args");
		return 1;
	}
	
	FILE *input_file;
	if (open_file(&input_file, argv[1], "rb") != OPEN_OK) {
		print_error("Opening file problem");
		return 1;
	}

	struct image *input_image = NULL;
	if (from_bmp(input_file, &input_image) != READ_OK) {
		print_error("Reading file problem");
		return 1;
	}

	close_file(input_file);
	
	struct image *output_image;
	output_image = rotate(input_image);
	
	FILE *output_file;
	if (open_file(&output_file, argv[2], "wb") != OPEN_OK) {
		print_error("Opening file problem");
		return 1;
	}
	
	if (to_bmp(output_file, output_image) != WRITE_OK) {
		print_error("Writing file problem");
		return 1;
	}
	
	close_file(output_file);
	delete_image(input_image);
	delete_image(output_image);
    return 0;
}
